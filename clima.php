<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/estilos.css">
    <title>Clima</title>
</head>

<body>
    <div class="main-container">
        
        <!-- Contenedor Banner -->
        <div class="banner">
            <img id="banner" src="img/bogota.jpg" alt="banner">
            <div class="btaweather">
                <?php
                    include 'php/weather.php';
                ?>
                <?php
                    include 'php/weatherParis.php';
                ?>
            </div>
        </div>

        <!-- Contenedor 3 Days Forecast -->
        <div class="forecast">
            <div class="threrD">
                <h1 id='three'>3 Days Forecast</h1>
                
                <!-- Tabla DIA UNO -->
                <div class="diaUno">
                    <?php
                        require_once 'php/forecastM.php';
                        echo "<table id=''>";
                        echo    "<tr>";
                        echo        "<td>";
                        echo            "<table>";
                        echo                "<tr>";
                        echo                    "<td rowspan='2'>";
                        echo                    "$ForeCast1_icono";
                        echo                    "</td>";
                        echo                    "<td>";
                                                echo "$ForeCast1_dia";
                        echo                    "</th>";
                        echo                "</tr>";
                        echo                "<tr>";
                        echo                    "<td>$ForeCast1_descripcion</td>";
                        echo                "</tr>";
                        echo            "</table>";
                        echo            "<td id='temper'>"
                                            .number_format($miTemperaturaMinima, 0)."°/"
                                            .number_format($miTemperaturaMaxima, 0)."°
                                        </td>";
                        echo        "</td>";
                        echo    "</tr>";
                        echo "</table>";
                    ?>
                </div>
                
                <!-- Tabla DIA DOS -->
                <div class="diaDos">
                    <?php
                        require_once 'php/forecastM.php';
                        echo "<table id=''>";
                        echo    "<tr>";
                        echo        "<td>";
                        echo            "<table>";
                        echo                "<tr>";
                        echo                    "<td rowspan='2'>";
                        echo                    "$ForeCast2_icono";
                        echo                    "</td>";
                        echo                    "<td>";
                                                echo "$ForeCast2_dia";
                        echo                    "</th>";
                        echo                "</tr>";
                        echo                "<tr>";
                        echo                    "<td>$ForeCast2_descripcion</td>";
                        echo                "</tr>";
                        echo            "</table>";
                        echo            "<td id='temper2'>"
                                            .number_format($ForeCast2_tempMinima, 0)."°/"
                                            .number_format($ForeCast2_tempMaxima, 0)."°
                                        </td>";
                        echo        "</td>";
                        echo    "</tr>";
                        echo "</table>";
                    ?>
                </div>

                <!-- Tabla DIA TRES -->
                <div class="diaTres">
                    <?php
                        require_once 'php/forecastM.php';
                        echo "<table id=''>";
                        echo    "<tr>";
                        echo        "<td>";
                        echo            "<table>";
                        echo                "<tr>";
                        echo                    "<td rowspan='2'>";
                        echo                    "$ForeCast3_icono";
                        echo                    "</td>";
                        echo                    "<td>";
                                                echo "$ForeCast3_dia";
                        echo                    "</th>";
                        echo                "</tr>";
                        echo                "<tr>";
                        echo                    "<td>$ForeCast3_descripcion</td>";
                        echo                "</tr>";
                        echo            "</table>";
                        echo            "<td id='temper2'>"
                                            .number_format($ForeCast3_tempMinima, 0)."°/"
                                            .number_format($ForeCast3_tempMaxima, 0)."°
                                        </td>";
                        echo        "</td>";
                        echo    "</tr>";
                        echo "</table>";
                    ?>
                </div>
            </div>
            <!-- <img id="torre" src="img/img1.jpg" alt=""> -->
            <div class="visit">
                <h1 id="three">Place to Visit</h1>
                <img id="visit" src="img/img1.jpg" alt="">
            </div>
            
            <div class="top">
                <h1 id="three">+ Top Reviews</h1>
                <!-- <img id="uno" src="img/top.jpg" alt=""> -->
                <img id="uno" src="img/img2.jpg" alt="">
                <img id="dos" src="img/img3.jpg" alt="">
            </div>
            <div class="lyon">
            <img id="lyon" src="img/img5.jpg" alt="">
            <img id="location" src="img/img4.jpg" alt="">
            </div>
        </div>
    </div>
</body>
</html>