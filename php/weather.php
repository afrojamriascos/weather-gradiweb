<?php

    //Datos de Bogotá
    // echo "<h1>Bogot&aacute;</h1>";
    $cityID = "3688689"; //3689147 -- 3688689 -- 3564073
    $apiKey = "63e473a76e299715557b9c29844b1ca0";
    $json = file_get_contents("https://api.openweathermap.org/data/2.5/weather?id=" . $cityID . "&appid=" . $apiKey . "&units=metric");
    $objBogota = json_decode($json,true);

    //clima de Bogotá
    $weatherBta = $objBogota['weather'];
    $miTextoClimaBogota = $weatherBta[0]['main'];
    $miIconoClimaBogota = $weatherBta[0]['icon'];

    //temperatura de Bogotá
    $tempBogota = json_decode($json);
    $miTemperaturaBogota = $tempBogota->{'main'}->temp;

    echo "<div id='divClimaBta'>";
    echo "<h1>Bogot&aacute;</h1>";
    echo "<img src='http://openweathermap.org/img/wn/".$miIconoClimaBogota."@2x.png'>";
    echo "<label>".$miTextoClimaBogota."</label>"   ;
    echo "<h2>".number_format($miTemperaturaBogota, 0)."°</h2>"   ;
    echo "</div>";
    //echo (print_r($objBogota,true));

?>