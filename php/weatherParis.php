<?php

    //Datos de París
    // echo "<h1>Par&iacute;s</h1>";
    $cityID = "2968815";
    $apiKey = "63e473a76e299715557b9c29844b1ca0";
    $json = file_get_contents("https://api.openweathermap.org/data/2.5/weather?id=" . $cityID . "&appid=" . $apiKey . "&units=metric");
    $objParis = json_decode($json,true);


    //clima de París
    $weatherParis = $objParis['weather'];
    $miTextoClimaParis = $weatherParis[0]['main'];
    $miIconoClimaParis = $weatherParis[0]['icon'];

    //temperatura de París
    $tempParis =  json_decode($json);
    $miTemperaturaParis = $tempParis->{'main'}->temp;

    echo "<div id='divClimaPrs'>";
    echo "<h1>Par&iacute;s</h1>";
    echo "<img src='http://openweathermap.org/img/wn/".$miIconoClimaParis."@2x.png'>";
    echo "<label>".$miTextoClimaParis."</label>"   ;
    echo "<h2>".number_format($miTemperaturaParis, 0)."°</h2>"   ;
    echo "</div>";
    //echo(print_r($objParis,true));

?>