<?php

//Datos de Bogotá
// echo "<h1>Info Bogot&aacute;</h1>";
$cityID = "3688689";
$apiKey = "63e473a76e299715557b9c29844b1ca0";
$json = file_get_contents("https://api.openweathermap.org/data/2.5/forecast?id=".$cityID."&appid=".$apiKey."&units=metric");
$objPronosticoBogota = json_decode($json,true);

$miFechaActual='XXXX-XX-XX';
$arrPronostico = array();
$lista = $objPronosticoBogota["list"];
$contadorDias = 0;

$ForeCast1_icono;
$ForeCast1_dia ="";
$ForeCast1_descripcion ="";
$ForeCast1_tempMinima ="";
$ForeCast1_tempMaxima ="";

$ForeCast2_icono ="";
$ForeCast2_dia ="";
$ForeCast2_descripcion ="";
$ForeCast2_tempMinima ="";
$ForeCast2_tempMaxima ="";

$ForeCast3_icono ="";
$ForeCast3_dia ="";
$ForeCast3_descripcion ="";
$ForeCast3_tempMinima ="";
$ForeCast3_tempMaxima ="";

$hoy = date("Y-m-d") ;

for ($i=0; $i < count($lista); $i++){
    $miItem = $lista[$i];
    $miPrincipal = $miItem["main"];
    $miFecha = $miItem["dt_txt"];
    $miTemperaturaMinima = $miPrincipal["temp_min"];
    $miTemperaturaMaxima = $miPrincipal["temp_max"];
    
    $miClima = $miItem["weather"];
    $miDescripcion = $miClima[0]["description"];
    $miIcono = $miClima[0]["icon"];
    
    //de la fecha recuperada de forecast se toman los 10 primeros caracteres
    //que corresponden a dicha fecha
    $miYYYYMMDD = substr($miFecha,0,10);
    
    //si la fecha no es hoy, calcular valores para mostrar en pantalla
    if ($miYYYYMMDD != $hoy ){
   
		$unixTimestamp = strtotime($miYYYYMMDD);
		$dayOfWeek = date("l", $unixTimestamp);
		
		if ($miYYYYMMDD != $miFechaActual) {
			$contadorDias++;
			if ($contadorDias<=3){
				
				if ($contadorDias==1){
					$ForeCast1_icono ="<img src='http://openweathermap.org/img/wn/".$miIcono.".png'>";
					$ForeCast1_dia = $dayOfWeek;
					$ForeCast1_descripcion =$miDescripcion;
					number_format($ForeCast1_tempMinima=$miTemperaturaMinima, 0) ."°";
					number_format($ForeCast1_tempMaxima=$miTemperaturaMaxima, 0) ."°";				
				}
				
				if ($contadorDias==2){
					$ForeCast2_icono ="<img src='http://openweathermap.org/img/wn/".$miIcono.".png'>";
					$ForeCast2_dia = $dayOfWeek;
					$ForeCast2_descripcion =$miDescripcion;
					number_format($ForeCast2_tempMinima=$miTemperaturaMinima, 0) ."°";
					number_format($ForeCast2_tempMaxima=$miTemperaturaMaxima, 0) ."°";				
				}			

				if ($contadorDias==3){
					$ForeCast3_icono ="<img src='http://openweathermap.org/img/wn/".$miIcono.".png'>";
					$ForeCast3_dia = $dayOfWeek;
					$ForeCast3_descripcion =$miDescripcion;
					number_format($ForeCast3_tempMinima=$miTemperaturaMinima, 0) ."°";
					number_format($ForeCast3_tempMaxima=$miTemperaturaMaxima, 0) ."°";				
				}	
				
				$miFechaActual = $miYYYYMMDD;
				
			}//fin.if ($contadorDias<=3)
			
		}//fin.if ($miYYYYMMDD != $miFechaActual)

  	} //fin.if ($miYYYYMMDD != $hoy )
}

?>